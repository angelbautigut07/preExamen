/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen;

import javax.swing.JOptionPane;

/**
 *
 * @author angel
 */
public class jintRegistroVenta extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintRegistroVenta
     */
    public jintRegistroVenta() {
        initComponents();
        this.resize(685, 485);
        this.deshablititar();
    }

    
    public void habilitar(){
        this.txtCantidad.setEnabled(!false);
        this.txtCodigoVenta.setEnabled(!false);
        this.cmbTipo.setEnabled(!false);
        
        this.btnGuardar.setEnabled(!false);
        this.btnCalcular.setEnabled(!false);
    }
    
    public void deshablititar(){
        this.txtCantidad.setEnabled(false);
        this.txtCodigoVenta.setEnabled(false);
        this.txtCostoVenta.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.txtTotalAPagar.setEnabled(false);
        this.cmbTipo.setEnabled(false);
        
        this.btnGuardar.setEnabled(false);
        this.btnCalcular.setEnabled(false);
    }
    
     public void limpiar(){
        this.txtCantidad.setText("");
        this.txtCodigoVenta.setText("");
        this.txtCostoVenta.setText("");
        this.txtImpuesto.setText("");
        this.txtPrecio.setText("");
        this.txtTotalAPagar.setText("");
        
        //poner en la primera de posicion al combobox
        this.cmbTipo.setSelectedIndex(0);
        
        //Poner el cursor en el numCotizacion
        this.txtCodigoVenta.requestFocus();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtCodigoVenta = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cmbTipo = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtTotalAPagar = new javax.swing.JTextField();
        txtCostoVenta = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        btnCalcular = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Codigo de Venta:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 30, 130, 30);

        txtCodigoVenta.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txtCodigoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodigoVentaActionPerformed(evt);
            }
        });
        getContentPane().add(txtCodigoVenta);
        txtCodigoVenta.setBounds(170, 30, 270, 30);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Cantidad: ");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(90, 80, 80, 30);

        txtCantidad.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txtCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCantidadActionPerformed(evt);
            }
        });
        getContentPane().add(txtCantidad);
        txtCantidad.setBounds(170, 80, 270, 30);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Tipo:  ");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(120, 130, 50, 30);

        txtPrecio.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txtPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioActionPerformed(evt);
            }
        });
        getContentPane().add(txtPrecio);
        txtPrecio.setBounds(170, 180, 110, 30);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Precio: ");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(110, 180, 60, 30);

        cmbTipo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Regular", "Premium" }));
        cmbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoActionPerformed(evt);
            }
        });
        getContentPane().add(cmbTipo);
        cmbTipo.setBounds(170, 130, 110, 30);

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos del Registro de Venta", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 16))); // NOI18N
        jPanel1.setLayout(null);

        jLabel9.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Total a Pagar: ");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(40, 100, 110, 30);

        jLabel11.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("Costo de Venta: ");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(30, 20, 120, 30);

        jLabel12.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("Impuesto: ");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(70, 60, 80, 30);

        txtTotalAPagar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtTotalAPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalAPagarActionPerformed(evt);
            }
        });
        jPanel1.add(txtTotalAPagar);
        txtTotalAPagar.setBounds(160, 100, 340, 30);

        txtCostoVenta.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtCostoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCostoVentaActionPerformed(evt);
            }
        });
        jPanel1.add(txtCostoVenta);
        txtCostoVenta.setBounds(160, 20, 340, 30);

        txtImpuesto.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });
        jPanel1.add(txtImpuesto);
        txtImpuesto.setBounds(160, 60, 340, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(50, 230, 550, 150);

        btnCalcular.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnCalcular.setText("Calcular");
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        getContentPane().add(btnCalcular);
        btnCalcular.setBounds(500, 150, 100, 40);

        btnNuevo.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(500, 30, 100, 40);

        btnGuardar.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(500, 90, 100, 40);

        btnLimpiar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(50, 390, 110, 40);

        btnCancelar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(280, 390, 110, 40);

        btnCerrar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(490, 390, 110, 40);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.jpg"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(-250, -70, 1000, 530);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCodigoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoVentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoVentaActionPerformed

    private void txtCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantidadActionPerformed

    private void txtPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioActionPerformed

    private void txtTotalAPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalAPagarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalAPagarActionPerformed

    private void txtCostoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCostoVentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCostoVentaActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
        // TODO add your handling code here:
         this.txtCantidad.setText(String.valueOf(reg.getCantidad()));
         this.txtPrecio.setText(String.valueOf(reg.getPrecio()));
         this.txtCodigoVenta.setText(String.valueOf(reg.getCodigoVenta()));
         
         switch(reg.getTipo()){
             case 0: this.cmbTipo.setSelectedIndex(0); break;
             case 1: this.cmbTipo.setSelectedIndex(1); break;
         }
         
         this.txtCostoVenta.setText(String.valueOf(reg.calcularCostoVenta()));
         this.txtImpuesto.setText(String.valueOf(reg.calcularImpuesto()));
         this.txtTotalAPagar.setText(String.valueOf(reg.calcularTotalAPagar()));
         
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        reg = new RegistroVenta();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
         boolean exito = false;
        if(this.txtCodigoVenta.getText().equals("")) exito = true;
        if(this.txtCantidad.getText().equals("")) exito = true;

        if(exito == true){
            //falto informacion
            JOptionPane.showMessageDialog(this, "Falto capturar información");
        }
        else {
            //todo bien
            reg.setCodigoVenta(Integer.parseInt(this.txtCodigoVenta.getText()));
            reg.setCantidad(Float.parseFloat(this.txtCantidad.getText()));
            switch(this.cmbTipo.getSelectedIndex()){
            case 0:
               reg.setTipo(0);
               reg.setPrecio(17.50f);
               break;
            case 1:
               reg.setTipo(1);
               reg.setPrecio(18.50f);
               break;
        }
           
            JOptionPane.showMessageDialog(this, "Se guardó la informacion con exito");
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshablititar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;

        opcion = JOptionPane.showConfirmDialog(this, "¿Realmente quieres cerrar?", "Cotizacion", JOptionPane.YES_NO_OPTION);

        if (opcion == JOptionPane.YES_OPTION){

            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void cmbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbTipoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigoVenta;
    private javax.swing.JTextField txtCostoVenta;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtTotalAPagar;
    // End of variables declaration//GEN-END:variables
    private RegistroVenta reg;
}
