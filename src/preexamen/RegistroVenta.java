/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen;

/**
 *
 * @author angel
 */
public class RegistroVenta {
    private int codigoVenta;
    private float cantidad;
    private float precio;
    private int tipo;

    public RegistroVenta() {
        this.codigoVenta = 0;
        this.cantidad = 0.0f;
        this.precio = 0.00f;
        this.tipo = 0;
    }

    public RegistroVenta(int codigoVenta, float cantidad, float precio, int tipo) {
        this.codigoVenta = codigoVenta;
        this.cantidad = cantidad;
        this.precio = precio;
        this.tipo = tipo;
    }
    

    public RegistroVenta(RegistroVenta x) {
        this.codigoVenta = x.codigoVenta;
        this.cantidad = x.cantidad;
        this.precio = x.precio;
        this.tipo = x.tipo;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float calcularCostoVenta(){
        float costo = 0.0f;
        costo = this.cantidad * this.precio;
        return costo;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularCostoVenta() * 0.16f;
        return impuesto;
    }
    
    public float calcularTotalAPagar(){
        float total = 0.0f;
        total = this.calcularImpuesto() + this.calcularCostoVenta();
        return total;
    }
    
    public void imprimirRegistro(){
        System.out.println("Codigo de Venta: " + this.codigoVenta);
        System.out.println("Cantidad: " + this.cantidad);
        if (tipo == 0){
            System.out.println("Tipo: Regular");
            System.out.println("Precio: $17.50");
        } 
        else {
        System.out.println("Tipo: Premium");
        System.out.println("Precio: $18.50");
        }
        System.out.println("Costo de Venta: " + this.calcularCostoVenta());
        System.out.println("Impuesto: " + this.calcularImpuesto());
        System.out.println("Total a Pagar: " + this.calcularTotalAPagar());
    }
  
    
    
}
